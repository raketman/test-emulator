<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

abstract class BaseAdmin extends AbstractAdmin
{
    const LABEL_NAME = 'Название';
    const LABEL_CODE = 'Код';
    const LABEL_TYPE = 'Тип';
    const LABEL_ACTIVE = 'Активность';

    protected $translationDomain = 'TestEmulatorAdmin';
}