<?php
namespace App\Admin;

use App\Entity\ProjectClient;
use App\Traits\AdminSideMenuTrait;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type as SonataFormTypes;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ClientAdmin extends BaseAdmin
{

    use AdminSideMenuTrait;

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
            ->tab('Основные') // the tab call is optional
            ->with('Аттрибуты')
            ->add('name', TextType::class, ['label' => 'Название'])
            ->add('basic_login', TextType::class, ['label' => 'Логин'])
            ->add('basic_password', TextType::class, ['label' => 'Пароль'])
            ->end()
            ->end()
            ->tab('Доступ к проектам')
            ->add('projectClients', CollectionType::class, array(
                'label'                 => ' ',
                // 'sonata_admin'          => 'admin.client_proojects',
                'btn_add' => 'Добавить',
                'block_name'    => 'Связь с проектом'
            ))

            ->end();
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Основные') // the tab call is optional
                ->with('Аттрибуты')
                        ->add('name', TextType::class, ['label' => 'Название'])
                        ->add('basic_login', TextType::class, ['label' => 'Логин'])
                        ->add('basic_password', TextType::class, ['label' => 'Пароль'])
                ->end()
            ->end()

            ->tab('Доступ к проектам')
            ->add('projectClients', CollectionType::class, array(
                'label'                 => ' ',
               // 'sonata_admin'          => 'admin.client_proojects',
                'btn_add' => 'Добавить',
                'block_name'    => 'Связь с проектом'
            ), [
                'edit'  => 'inline'
            ])

            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => 'Название']);
        $datagridMapper->add('basic_login', null, ['label' => 'Логин']);
        $datagridMapper->add('basic_password', null, ['label' => 'Пароль']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => 'Название']);
        $listMapper->add('basic_login', TextType::class, ['label' => 'Логин']);
        $listMapper->add('basic_password', TextType::class, ['label' => 'Пароль']);
        $listMapper->add('_action', null, [
            'actions' => ['show'  => []]
        ]);
    }
}