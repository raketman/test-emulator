<?php
namespace App\Admin;

use App\Traits\AdminSideMenuTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ProjectAdmin extends BaseAdmin
{
    use AdminSideMenuTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class, ['label' => 'Название']);
        $formMapper->add('code', TextType::class, ['label' => 'Код']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => 'Название']);
        $datagridMapper->add('code', null, ['label' => 'Код']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => 'Название']);
        $listMapper->addIdentifier('code', TextType::class, ['label' => 'Код']);
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
            ->tab('Основные')
            ->with('Аттрибуты')
            ->add('name', TextType::class, ['label' => 'Название'])
            ->add('code', TextType::class, ['label' => 'Код'])
            ->end()
            ->end()
//            ->tab('Сервисы')
//            ->add('projectServices', CollectionType::class, array(
//                'label'                 => ' ',
//                'btn_add' => 'Добавить',
//                'block_name'    => 'Связь с проектом'
//            ))
//
//            ->end()
        ;
    }
}