<?php
namespace App\Admin;

use App\Traits\AdminChildTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

final class ProjectClientAdmin extends BaseAdmin
{
    use AdminChildTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('client', null, ['label' => 'Клиент']);
        $formMapper->add('project', null, ['label' => 'Проект']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('client', null, ['label' => 'Клиент']);
        $listMapper->addIdentifier('project',  null, ['label' => 'Проект']);
    }
}