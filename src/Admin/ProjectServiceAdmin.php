<?php
namespace App\Admin;

use App\Traits\AdminChildTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



final class ProjectServiceAdmin extends BaseAdmin
{
    use AdminChildTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('service', null, ['label' => 'Сервис']);
        $formMapper->add('project', null, ['label' => 'Проект']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('service', null, ['label' => 'Сервис']);
        $listMapper->add('project',  null, ['label' => 'Проект']);
    }
}