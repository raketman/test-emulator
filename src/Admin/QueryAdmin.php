<?php
namespace App\Admin;

use App\Traits\AdminChildTrait;
use App\Traits\AdminSideMenuTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\DataTransformer\ModelsToArrayTransformer;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class QueryAdmin extends BaseAdmin
{
    const LABEL_URI = 'Адрес';

    use AdminChildTrait;
    use AdminSideMenuTrait;

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
            ->tab('Основные')
            ->with('Аттрибуты')
            ->add('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('uri', TextType::class, ['required' => false, 'label' => self::LABEL_URI])
            ->add('service')
            ->end()
            ->end()
            ->tab('Критерии')
            ->add(
                'filters',
                CollectionType::class,
                [
                    ///'btn_add'           => false,
                    //'btn_catalogue'   => 'Новый',
                    //'by_reference' => false
                ],
                [
                    'edit'  => 'table',
                    'inline' => 'inline',
                    'sortable' => 'position',
                ]
            )
            ->end()
        ;
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('uri', TextType::class, ['required' => false, 'label' => self::LABEL_URI])
            ->add('active', null, ['label' => self::LABEL_ACTIVE])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => self::LABEL_NAME]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('uri', TextType::class, ['label' => self::LABEL_URI])
            ->add('active', null, ['label' => self::LABEL_ACTIVE]);


        $listMapper
            ->add('_action', null, [
            'actions' => [
                'childadmin' => [
                    'template' => '@App/Query/list__action_childadmin.html.twig',
                ],
            ]
        ]);

    }
}