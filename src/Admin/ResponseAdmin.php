<?php
namespace App\Admin;

use App\Traits\AdminChildTrait;
use App\Traits\AdminSideMenuTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type as SATypes;

final class ResponseAdmin extends BaseAdmin
{
    const LABEL_DEFAULT = 'Показывать в любой случае, если не выбраны другие';
    const LABEL_VALUE = 'Значение';
    const LABEL_TIMEOUT = 'Время формирования ответа';
    const LABEL_BLOCKED_TIME = 'Время блокировки ответа в сек';
    const LABEL_BLOCKED_UNTIL = 'Дата до снятия блокировкм ответа';
    const LABEL_HEADERS = 'Заголовки';
    const LABEL_HTTP_STATUS = 'Код ответа';

    use AdminSideMenuTrait;
    use AdminChildTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('headers', TextareaType::class, ['attr' => ['rows' => 5],'label' => self::LABEL_HEADERS])
            ->add('http_status', TextType::class, ['label' => self::LABEL_HTTP_STATUS])
            ->add('value', TextareaType::class, ['attr' => ['rows' => 15],'label' => self::LABEL_VALUE])
            ->add('timeout', TextType::class, ['label' => self::LABEL_TIMEOUT])
            ->add('blocked_time', TextType::class, ['label' => self::LABEL_BLOCKED_TIME])
            ->add('blocked_until', null, ['required' => false,'label' => self::LABEL_BLOCKED_UNTIL])
            ->add('is_default', null, ['label' => self::LABEL_DEFAULT])
            ->add('active', null, ['label' => self::LABEL_ACTIVE]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => self::LABEL_NAME]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('active', null, ['label' => self::LABEL_ACTIVE]);

        $listMapper
            ->add('_action', null, [
                'actions' => [
                    'childadmin' => [
                        'template' => '@App/Response/list__action_childadmin.html.twig',
                    ],
                ]
            ]);
    }
}