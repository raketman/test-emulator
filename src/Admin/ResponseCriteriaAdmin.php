<?php
namespace App\Admin;

use App\Enum\ResponseCriteriaTypeList;
use App\Traits\AdminChildTrait;
use App\Traits\AdminSideMenuTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ResponseCriteriaAdmin extends BaseAdmin
{
    const LABEL_VALUE = 'Значение';

    use AdminChildTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('type', ChoiceType::class, [
                'label'     => self::LABEL_TYPE,
                'choices'   => ResponseCriteriaTypeList::getChoices()
            ])
            ->add('value', TextType::class, ['label' => self::LABEL_VALUE]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => self::LABEL_NAME]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('type', ChoiceType::class, [
                'label'     => self::LABEL_TYPE,
                'choices'   => ResponseCriteriaTypeList::getChoices()
            ])
            ->add('value', TextType::class, ['label' => self::LABEL_VALUE]);
    }
}