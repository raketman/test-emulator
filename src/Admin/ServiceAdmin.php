<?php
namespace App\Admin;

use App\Enum\ServiceTypeList;
use App\Traits\AdminSideMenuTrait;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
;

use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ServiceAdmin extends BaseAdmin
{
    const LABEL_DEFINITION = 'Определение';
    const LABEL_HEADERS = 'Заголовки';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('code', TextType::class, ['label' => self::LABEL_CODE])
            ->add('type', ChoiceType::class, [
                'choices'   => ServiceTypeList::getChoices(),
                'label'     => self::LABEL_TYPE
            ])
            ->add('headers', TextareaType::class, ['attr' => ['rows' => 5],'label' => self::LABEL_HEADERS])
            ->add('definition', TextareaType::class, [ 'required' => false, 'attr' => ['rows' => 15],'label' => self::LABEL_DEFINITION])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => self::LABEL_NAME]);
        $datagridMapper->add('code', null, ['label' => self::LABEL_CODE]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => self::LABEL_NAME]);
        $listMapper->add('code', TextType::class, ['label' => self::LABEL_CODE]);
        $listMapper->add('type', TextType::class, ['label' => self::LABEL_TYPE]);

        $listMapper
            ->add('_action', null, [
                'actions' => [
                    'childadmin' => [
                        'template' => '@App/Service/list__action_childadmin.html.twig',
                    ],
                ]
            ]);
    }


    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $targetAdmin = $this;
        $targetId = $id;

        if ($queryId = $admin->getRequest()->get('childId')) {

            $sonataQueryAdmin = $this->getChildren()['sonata.admin.query'];

            if ($responseId = $admin->getRequest()->get('childChildId')) {
                $targetAdmin = $sonataQueryAdmin->getChildren()['sonata.admin.response'];
                $targetId = $responseId;
            } else {
                $targetAdmin = $sonataQueryAdmin;
                $targetId = $queryId;
            }
        }

        if (!$targetAdmin) {
            return;
        }
        /** @var AdminInterface $child */
        foreach ($targetAdmin->getChildren() as $child) {
            $menu->addChild($child->getLabel(), [
                'uri' => $targetAdmin->generateUrl(sprintf('%s.list', $child->getCode()), ['id' => $targetId])
            ]);

        }

    }

}