<?php
namespace App\Admin;

use App\Enum\ServiceAttributeTypeList;
use App\Traits\AdminChildTrait;
use App\Traits\AdminSideMenuTrait;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ServiceAttributeAdmin extends BaseAdmin
{
    const LABEL_HEADERS = 'Заголовки';
    const LABEL_DEFINITION = 'Определение';

    use AdminChildTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('code', TextType::class, ['label' => self::LABEL_CODE])
            ->add('type', ChoiceType::class, [
                'choices'   => ServiceAttributeTypeList::getChoices(),
                'label'     => self::LABEL_TYPE
            ])
            ->add('headers', TextareaType::class, ['attr' => ['rows' => 5],'label' => self::LABEL_HEADERS])
            ->add('definition', TextareaType::class, ['attr' => ['rows' => 15],'label' => self::LABEL_DEFINITION]);

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => self::LABEL_NAME]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', TextType::class, ['label' => self::LABEL_NAME])
            ->add('service');
    }
}