<?php
namespace App\Controller;

use App\Entity\Service;
use App\Service\Processor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

/**
 * Class QueryController
 * @package App\Controller
 * @Route("/{client_credential}")
 */
class QueryController
{
    /** @var Processor  */
    private $processor;

    /**
     * QueryController constructor.
     * @param Processor $processor
     */
    public function __construct(Processor $processor)
    {
        $this->processor = $processor;
    }


    /**
     * Метод, который обрабатывае входищй ззапрос
     * @param Request $request
     * @param Service $service
     * @return Response
     * @throws \App\Exception\Processor\NotResolveProcessException
     *
     *
     * @Route(
     *     "/{project}/{service}/definition{uri}",
     *     requirements={"uri"=".+"}
     * )
     * @Route(
     *     "/{project}/{service}/definition",
     *     methods={"POST", "DELETE", "UDPATE", "OPTIONS", "PATCH"}
     * )
     * @Entity("service", expr="repository.findExistent(project, service)")
     * @Security("is_granted('definition', project)")
     */
    public function process(Request $request, Service $service)
    {
        // Уберем ненужные данных
        $request->attributes->set('service', null);
        $request->attributes->set('project', null);

        return $this->processor->process($service, $request);

    }
}
