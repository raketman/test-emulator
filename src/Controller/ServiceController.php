<?php
namespace App\Controller;

use App\Entity\Project;
use App\Entity\Service;
use App\Entity\ServiceAttribute;
use App\Service\Definition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ServiceController
 * @package App\Controller
 * @Route("/{client_credential}")
 */
class ServiceController
{


    /** @var Definition */
    private $definition;

    public function __construct(Definition $definition)
    {
        $this->definition = $definition;
    }

    /**
     *
     *  Выдает описание сервиса, например wsdl
     *
     * @Route(
     *     "/{project}/{service}/definition",
     *     methods={"GET"}
     * )
     * @Entity("service", expr="repository.findExistent(project, service)")
     * @Security("is_granted('definition', project)")
     *
     * @param Service $service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\UnsupportedServiceTypeException
     */
    public function definition(Service $service )
    {
        return $this->definition->processService($service);
    }


    /**
     *
     * Для описания аттрибутов сервиса, например xsd или методов
     *
     * @Route(
     *     "/{project}/{service}/attribute/{attribute}",
     *     methods={"GET"}
     * )
     * @Entity("serviceAttribute", expr="repository.findExistent(project, service, attribute)")
     * @Security("is_granted('definition', project)")
     *
     *
     * @param ServiceAttribute $serviceAttribute
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\UnsupportedServiceTypeException
     */
    public function attribute(ServiceAttribute $serviceAttribute )
    {
        return $this->definition->processAttribute($serviceAttribute);
    }
}
