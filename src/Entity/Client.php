<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Клиенты, которые обращаются к данному сервису
 * Class Project
 * @ORM\Entity()
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(name="basic_login",columns={"basic_login"})}
 * )
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @package App\Entity
 */
class Client implements UserInterface
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;



    /**
     * @ORM\Column(type="string", length=50)
     */
    private $basic_login;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $basic_password;

    /**
     * Связь с проектом.
     * @ORM\OneToMany(targetEntity="ProjectClient", mappedBy="client", cascade={"persist"}, orphanRemoval=true)
     * @var ProjectClient[]
     *
     */
    private $projectClients;


    public function __construct()
    {
        $this->projectClients = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBasicLogin()
    {
        return $this->basic_login;
    }

    /**
     * @param mixed $basic_login
     */
    public function setBasicLogin($basic_login): void
    {
        $this->basic_login = $basic_login;
    }



    /**
     * @return mixed
     */
    public function getBasicPassword()
    {
        return $this->basic_password;
    }

    /**
     * @param mixed $basic_password
     */
    public function setBasicPassword($basic_password): void
    {
        $this->basic_password = $basic_password;
    }

    /**
     * @return ProjectClient[]
     */
    public function getProjectClients()
    {
        return $this->projectClients;
    }

    /**
     * @param ProjectClient[] $projectClients
     */
    public function setProjectClients(array $projectClients): void
    {
        $this->projectClients = $projectClients;
    }

    public function addProjectClient(ProjectClient $projectClient)
    {
        if (!$this->projectClients->contains($projectClient)) {
            $projectClient->setClient($this);
            $this->projectClients->add($projectClient);
        }

        return $this;
    }

    public function removeProjectClient($projectClient)
    {
        if ($this->projectClients->contains($projectClient)) {
            $this->projectClients->remove($projectClient);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name ?? '';
    }

    // Методы для реализация интерфайса пользователя

    public function getPassword()
    {
        return $this->getBasicPassword();
    }

    public function getRoles()
    {
        return [];
    }

    public function getUsername()
    {
        return $this->getBasicLogin();
    }

    public function getSalt()
    {
        return '';
    }

    public function eraseCredentials()
    {
        return null;
    }

}