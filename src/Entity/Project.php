<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Класс для описания проекта, в котором реализовные сервисы
 * Class Project
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(name="code",columns={"code"})}
 * )
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @package App\Entity
 */
class Project
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * Сервисы.
     * @ORM\OneToMany(targetEntity="ProjectService", mappedBy="project")
     * @var ProjectService[]
     *
     */
    private $projectServices;


    public function __construct()
    {
        $this->projectServices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }


    public function __toString()
    {
        return $this->name ?? '';
    }

    /**
     * @return ProjectService[]
     */
    public function getProjectServices()
    {
        return $this->projectServices;
    }

    /**
     * @param ProjectService[] $projectServices
     */
    public function setProjectServices(array $projectServices): void
    {
        $this->projectServices = $projectServices;
    }



}