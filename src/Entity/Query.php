<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Class Query
 * @ORM\Entity()
 * @package App\Entity
 */
class Query
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uri;

    /**
     * @ORM\Column(type="boolean", options={"default"=1})
     */
    private $active;

    /**
     * Запросы в сервисе.
     * @ORM\OneToMany(targetEntity="QueryCriteria", mappedBy="query", cascade="persist")
     * @var QueryCriteria[]
     *
     */
    private $filters;

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="queries")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * Запросы в сервисе.
     * @ORM\OneToMany(targetEntity="Response", mappedBy="query", cascade="persist")
     * @var Response[]
     *
     */
    private $responses;


    public function __construct()
    {
        $this->filters = new ArrayCollection();
        $this->responses= new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }


    /**
     * @return Collection|QueryCriteria[]
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    /**
     * @param QueryCriteria[] $filters
     */
    public function setFilters(array $filters): void
    {
        foreach ($filters as $filter) {
            $filter->setQuery($this);
        }

        $this->filters = $filters;
    }

    public function addFilter(QueryCriteria $criteria) {
        $criteria->setQuery($this);
        $this->filters->add($criteria);
        return $this;
    }

    public function removeFilter(QueryCriteria $criteria)
    {
        $this->filters->remove($criteria);
    }


    public function __toString()
    {
        return $this->name ?? '';
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return Response[]
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @param Response[] $responses
     */
    public function setResponses(array $responses): void
    {
        foreach ($responses as $response) {
            $response->setQuery($this);
        }

        $this->responses = $responses;
    }


    public function addResponse(Response $response)
    {
        $response->setQuery($this);
        $this->responses->add($response);

        return $this;
    }

    public function removeResponse(Response $response)
    {
        $this->responses->remove($response);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service): void
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }






}