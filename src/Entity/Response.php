<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Class Response
 * @ORM\Entity()
 * @package App\Entity
 */
class Response
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;


    /**
     * @ORM\Column(type="integer")
     */
    private $timeout;


    /**
     * Время блокировки после использования
     * @ORM\Column(type="integer", nullable=true)
     */
    private $blocked_time;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $blocked_until;

    /**
     * @ORM\Column(type="text")
     */
    private $headers;

    /**
     * @ORM\Column(type="text")
     */
    private $http_status;

    /**
     * @ORM\Column(type="text")
     */
    private $value;


    /**
     * @ORM\Column(type="boolean", options={"default"=1})
     */
    private $active;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $is_default;

    /**
     * @ORM\ManyToOne(targetEntity="Query", inversedBy="responses")
     * @ORM\JoinColumn(name="query_id", referencedColumnName="id")
     */
    private $query;

    /**
     * Запросы в сервисе.
     * @ORM\OneToMany(targetEntity="ResponseCriteria", mappedBy="response", cascade="persist")
     * @var ResponseCriteria[]
     *
     */
    private $filters;

    public function __construct()
    {
        $this->filters = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->name ?? '';
    }

    /**
     * @return mixed
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param mixed $timeout
     */
    public function setTimeout($timeout): void
    {
        $this->timeout = $timeout;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getHttpStatus()
    {
        return $this->http_status;
    }

    /**
     * @param mixed $http_status
     */
    public function setHttpStatus($http_status): void{
        $this->http_status = $http_status;
    }


    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query): void
    {
        $this->query = $query;
    }

    /**
     * @return Collection|ResponseCriteria[]
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    /**
     * @param ResponseCriteria[] $filters
     */
    public function setFilters(array $filters): void
    {
        foreach ($filters as $filter) {
            $filter->setResponse($this);
        }

        $this->filters = $filters;
    }

    public function addFilter(ResponseCriteria $criteria) {
        $criteria->setResponse($this);
        $this->filters->add($criteria);
        return $this;
    }

    public function removeFilter(ResponseCriteria $criteria)
    {
        $this->filters->remove($criteria);
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getBlockedTime()
    {
        return $this->blocked_time;
    }

    /**
     * @param mixed $blocked_time
     */
    public function setBlockedTime($blocked_time): void
    {
        $this->blocked_time = $blocked_time;
    }

    /**
     * @return null|\DateTime
     */
    public function getBlockedUntil()
    {
        return $this->blocked_until;
    }

    /**
     * @param \DateTime $blocked_until
     */
    public function setBlockedUntil(\DateTime $blocked_until = null): void
    {
        $this->blocked_until = $blocked_until;
    }

    /**
     * @return mixed
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * @param mixed $is_default
     */
    public function setIsDefault($is_default): void
    {
        $this->is_default = $is_default;
    }



    public function isActive()
    {
        return $this->active && (!$this->blocked_until || $this->blocked_until < new \DateTime());
    }







}