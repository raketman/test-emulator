<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Sonata\Form\Type\CollectionType;


/**
 * Class Service
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @package App\Entity
 */
class Service
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $headers;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $definition;

    /**
     * Запросы в сервисе.
     * @ORM\OneToMany(targetEntity="Query", mappedBy="service", cascade="persist")
     * @var ServiceQuery[]
     *
     */
    private $queries;

    /**
     * Запросы в сервисе.
     * @ORM\OneToMany(targetEntity="ServiceAttribute", mappedBy="service")
     * @var ServiceAttribute[]
     *
     */
    private $serviceAttributes;


    /**
     * Сервисы.
     * @ORM\OneToMany(targetEntity="ProjectService", mappedBy="service")
     * @var ProjectService[]
     *
     */
    private $projectServices;



    public function __construct()
    {
        $this->projectServices      = new ArrayCollection();
        $this->queries              = new ArrayCollection();
        $this->serviceAttributes    = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }


    /**
     * @return ServiceAttribute[]
     */
    public function getServiceAttributes(): array
    {
        return $this->serviceAttributes;
    }

    /**
     * @param ServiceAttribute[] $serviceAttributes
     */
    public function setServiceAttributes(array $serviceAttributes): void
    {
        $this->serviceAttributes = $serviceAttributes;
    }


    public function __toString()
    {
        return $this->name ?? '';
    }

    /**
     * @return ProjectService[]
     */
    public function getProjectServices()
    {
        return $this->projectServices;
    }

    /**
     * @param ProjectService[] $projectServices
     */
    public function setProjectServices(array $projectServices): void
    {
        $this->projectServices = $projectServices;
    }

    /**
     * @return mixed
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * @param mixed $definition
     */
    public function setDefinition($definition): void
    {
        $this->definition = $definition;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return Query[]
     */
    public function getQueries(): Collection
    {
        return $this->queries;
    }

    /**
     * @param Query[] $queries
     */
    public function setQueries(array $queries): void
    {
        $this->queries = $queries;
    }



    public function addQuery(Query $query)
    {
        $query->setService($this);
        $this->queries->add($query);
    }


    public function removeQuery(Query $query)
    {
        $this->queries->remove($query);
    }
}