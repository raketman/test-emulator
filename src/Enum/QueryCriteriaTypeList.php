<?php
namespace App\Enum;

class QueryCriteriaTypeList
{
    const REGEX = 'regex';

    private static $list = [
        self::REGEX  => self::REGEX,
    ];

    public static function getChoices()
    {
        return static::$list;
    }
}