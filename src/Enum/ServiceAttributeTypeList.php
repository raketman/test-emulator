<?php
namespace App\Enum;

class ServiceAttributeTypeList
{
    const XSD = 'xsd';
    const METHOD = 'method';

    private static $list = [
        self::XSD  => self::XSD,
        self::METHOD  => self::METHOD
    ];

    public static function getChoices()
    {
        return static::$list;
    }
}