<?php
namespace App\Enum;

class ServiceTypeList
{
    const SOAP = 'soap';
    const HTTP = 'http';

    private static $list = [
        self::SOAP  => self::SOAP,
        self::HTTP  => self::HTTP
    ];

    public static function getChoices()
    {
        return static::$list;
    }
}