<?php

namespace App\EventListener;


use App\Entity\Client;
use App\Traits\ErrorLoggingTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ExceptionListenerListener
{
    use ErrorLoggingTrait;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // Залогируем ошибку
        $this->logger->error($event->getThrowable()->getCode(), $this->formatException($event->getThrowable()));

        $event->setResponse(new Response('',  Response::HTTP_I_AM_A_TEAPOT));
    }
}
