<?php
namespace App\Helper\Crypt;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Класс для шифровки/расшифровки
 *
 * Class ClientCredentialCrypt
 * @package App\Helper\Crypt
 */
class ClientCredentialCrypt
{
    // TODO: подключть crypt
    public function __construct(ParameterBagInterface $parameterBag)
    {

    }

    public function encode($str)
    {
        return base64_encode($str);
    }

    public function decode($str)
    {
        return base64_decode($str);
    }
}