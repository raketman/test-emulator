<?php
namespace App\Helper\Definition;

use App\Entity\Service;
use App\Helper\ServiceDefinitionInterface;
use App\Traits\HeaderParseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SoapServiceDefinition
 * @package App\Service\Type
 */
class HttpServiceDefinition implements ServiceDefinitionInterface
{
    use ContainerAwareTrait;
    use ControllerTrait;
    use HeaderParseTrait;

    /** @var Service */
    protected $service;

    /** @var \Twig_Environment  */
    protected $twig;

    public function __construct(Service $service, \Twig_Environment $twig)
    {
        $this->service = $service;
        $this->twig = $twig;
    }

    /**
     * @param array $data
     * @return Response
     */
    public function generate(array $data = [])
    {
        return $this->render('@App/Definition/http.html.twig', [
            'service'   => $this->service, 'data' => $data
        ]);
    }
}