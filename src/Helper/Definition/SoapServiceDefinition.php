<?php
namespace App\Helper\Definition;

use App\Entity\Service;
use App\Helper\ServiceDefinitionInterface;
use App\Traits\HeaderParseTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SoapServiceDefinition
 * @package App\Service\Type
 */
class SoapServiceDefinition implements ServiceDefinitionInterface
{
    use HeaderParseTrait;

    /** @var Service */
    protected $service;

    /** @var \Twig_Environment  */
    protected $twig;

    public function __construct(Service $service, \Twig_Environment $twig)
    {
        $this->service = $service;
        $this->twig             = $twig;
    }

    /**
     * @param array $data
     * @return Response
     */
    public function generate(array $data = [])
    {
        $template = $this->twig->createTemplate($this->service->getDefinition());
        $response = new Response(
            $template->render($data),
            Response::HTTP_OK,
            $this->splitHeader($this->service->getHeaders())
        );

        return $response;
    }
}