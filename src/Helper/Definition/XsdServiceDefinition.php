<?php
namespace App\Helper\Definition;

use App\Entity\Service;
use App\Entity\ServiceAttribute;
use App\Helper\ServiceDefinitionInterface;
use App\Traits\HeaderParseTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SoapServiceDefinition
 * @package App\Service\Type
 */
class XsdServiceDefinition implements ServiceDefinitionInterface
{
    use HeaderParseTrait;

    /** @var ServiceAttribute */
    protected $serviceAttribute;

    /** @var \Twig_Environment */
    protected $twig;

    public function __construct(ServiceAttribute $serviceAttribute, \Twig_Environment $twig)
    {
        $this->serviceAttribute = $serviceAttribute;
        $this->twig             = $twig;
    }


    public function generate(array $data = [])
    {
        $template = $this->twig->createTemplate($this->serviceAttribute->getDefinition());
        $response = new Response(
            $template->render($data),
            Response::HTTP_OK,
            $this->splitHeader($this->serviceAttribute->getHeaders())
        );

        return $response;
    }
}