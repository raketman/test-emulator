<?php
namespace App\Helper;

use App\Entity\Service;
use App\Entity\ServiceAttribute;
use App\Enum\ServiceAttributeTypeList;
use App\Enum\ServiceTypeList;
use App\Exception\UnsupportedServiceTypeException;
use App\Helper\Definition\HttpServiceDefinition;
use App\Helper\Definition\SoapServiceDefinition;
use App\Helper\Definition\XsdServiceDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class ServiceDefinitionFactory
 * @package App\Service
 */
class ServiceDefinitionFactory
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        // TODO:  через compiler получить сервисы уюрать
        $this->twig = new \Twig_Environment(new \Twig_Loader_Array());
        $this->container = $container;
    }

    /**
     * @param Service $service
     * @return ServiceDefinitionInterface
     * @throws UnsupportedServiceTypeException
     */
    public function createServiceDefinition(Service $service) {
        switch ($service->getType()) {
            case ServiceTypeList::SOAP:
                // TODO  через compiler получить сервисы
                return new SoapServiceDefinition($service, $this->twig);
                break;
            case ServiceTypeList::HTTP:
                $handler =  new HttpServiceDefinition($service, $this->twig);
                $handler->setContainer($this->container);
                return $handler;
                break;
        }

        throw new UnsupportedServiceTypeException();
    }

    /**
     * @param Service $service
     * @return ServiceDefinitionInterface
     * @throws UnsupportedServiceTypeException
     */
    public function createAttributeDefinition(ServiceAttribute $attribute) {
        switch ($attribute->getType()) {
            case ServiceAttributeTypeList::XSD:
                // TODO  через compiler получить сервисы
                return new XsdServiceDefinition($attribute, $this->twig);
                break;
            case ServiceAttributeTypeList::METHOD:
                break;
        }

        throw new UnsupportedServiceTypeException();
    }
}