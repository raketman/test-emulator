<?php
namespace App\Helper;

use Symfony\Component\HttpFoundation\Response;

/**
 * Interface ServiceDefinitionInterface
 * @package App\Service
 */
interface ServiceDefinitionInterface
{
    /**
     * Метод отдает подготовленный ответ, содержиащий описания сервиса
     *
     * @param array $data
     * @return Response
     */
    public function generate(array $data = []);
}