<?php
namespace App\Helper\Twig;

use Twig\TwigFunction;

class HelperExtension extends \Twig_Extension {
    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('parse_url', 'parse_url'),
            new \Twig_SimpleFilter('json_decode', 'json_decode')
        );
    }
    public function getFunctions()
    {
        return array(
            new TwigFunction('print_r', array($this, 'print_r')),
            new TwigFunction('var_dump', array($this, 'var_dump')),
            new TwigFunction('json_view', array($this, 'json_view')),
            new TwigFunction('serialize', array($this, 'serialize')),
            new TwigFunction('stop', array($this, 'stop')),
            new TwigFunction('parse_str', array($this, 'parse_str')),

        );
    }

    public function parse_str($data)
    {
        parse_str($data, $result);
        return $result;
    }

    public function print_r($data)
    {
        print_r($data);
    }


    public function var_dump($data)
    {
        var_dump($data);
    }

    public function json_view($data)
    {
        echo json_encode($data);
    }

    public function serialize($data)
    {
        echo serialize($data);
    }

    public function stop($message)
    {
        die($message);
    }

    public function getName(){
        return "helper";
    }
}
