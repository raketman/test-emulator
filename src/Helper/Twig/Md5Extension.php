<?php
namespace App\Helper\Twig;

class Md5Extension extends \Twig_Extension {
    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('md5', 'md5'),
        );
    }
    public function getName(){
        return "md5";
    }
}