<?php
namespace App\Helper\View;


use App\Entity\Query;
use App\Entity\Response;
use App\Entity\Service;
use App\Helper\Twig\Base64Extension;
use App\Helper\Twig\HelperExtension;
use App\Helper\Twig\Md5Extension;
use App\Traits\CheckStrGenerateTrait;
use App\Traits\HeaderParseTrait;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response as HttpResponse;

class ResponseView
{
    use HeaderParseTrait;
    use CheckStrGenerateTrait;

    /**
     * @param Response $response
     * @param Query $query
     * @param Service $service
     * @param Request $request
     * @return HttpResponse
     */
    public function render(Response $response, Query $query, Service $service, Request $request)
    {
        // TODO вынести в сервис
        $twig = new \Twig_Environment(new \Twig_Loader_Array());
        $twig->addExtension(new Base64Extension());
        $twig->addExtension(new Md5Extension());
        $twig->addExtension(new HelperExtension());
        $template = $twig->createTemplate($response->getValue());

        return new HttpResponse(
            $template->render(array_merge(
                $this->getRequestData($request),
                $request->server->all()
            )),
            $response->getHttpStatus(),
            $this->splitHeader($response->getHeaders())
        );
    }
}