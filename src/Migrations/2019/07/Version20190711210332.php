<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190711210332 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE project_client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE project_service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE query_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE query_criteria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE response_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE response_criteria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_attribute_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE client (id INT NOT NULL, name VARCHAR(255) NOT NULL, basic_login VARCHAR(50) NOT NULL, basic_password VARCHAR(255) NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX basic_login ON client (basic_login)');
        $this->addSql('CREATE TABLE project (id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(50) NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX code ON project (code)');
        $this->addSql('CREATE TABLE project_client (id INT NOT NULL, client_id INT DEFAULT NULL, project_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D0E0EF1F19EB6921 ON project_client (client_id)');
        $this->addSql('CREATE INDEX IDX_D0E0EF1F166D1F9C ON project_client (project_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_row ON project_client (client_id, project_id)');
        $this->addSql('CREATE TABLE project_service (id INT NOT NULL, service_id INT DEFAULT NULL, project_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_778396B7ED5CA9E6 ON project_service (service_id)');
        $this->addSql('CREATE INDEX IDX_778396B7166D1F9C ON project_service (project_id)');
        $this->addSql('CREATE TABLE query (id INT NOT NULL, service_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, uri VARCHAR(255) DEFAULT NULL, active BOOLEAN DEFAULT \'true\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_24BDB5EBED5CA9E6 ON query (service_id)');
        $this->addSql('CREATE TABLE query_criteria (id INT NOT NULL, query_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_63357054EF946F99 ON query_criteria (query_id)');
        $this->addSql('CREATE TABLE response (id INT NOT NULL, query_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, timeout INT NOT NULL, blocked_time INT DEFAULT NULL, blocked_until TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, headers TEXT NOT NULL, http_status TEXT NOT NULL, value TEXT NOT NULL, active BOOLEAN DEFAULT \'true\' NOT NULL, is_default BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3E7B0BFBEF946F99 ON response (query_id)');
        $this->addSql('CREATE TABLE response_criteria (id INT NOT NULL, response_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1097DFCFBF32840 ON response_criteria (response_id)');
        $this->addSql('CREATE TABLE service (id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(50) NOT NULL, type VARCHAR(50) NOT NULL, headers TEXT DEFAULT NULL, definition TEXT DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE service_attribute (id INT NOT NULL, service_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(50) NOT NULL, type VARCHAR(50) NOT NULL, headers TEXT DEFAULT NULL, definition TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A2EBD5B1ED5CA9E6 ON service_attribute (service_id)');
        $this->addSql('ALTER TABLE project_client ADD CONSTRAINT FK_D0E0EF1F19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_client ADD CONSTRAINT FK_D0E0EF1F166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_service ADD CONSTRAINT FK_778396B7ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_service ADD CONSTRAINT FK_778396B7166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE query ADD CONSTRAINT FK_24BDB5EBED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE query_criteria ADD CONSTRAINT FK_63357054EF946F99 FOREIGN KEY (query_id) REFERENCES query (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE response ADD CONSTRAINT FK_3E7B0BFBEF946F99 FOREIGN KEY (query_id) REFERENCES query (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE response_criteria ADD CONSTRAINT FK_1097DFCFBF32840 FOREIGN KEY (response_id) REFERENCES response (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_attribute ADD CONSTRAINT FK_A2EBD5B1ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE project_client DROP CONSTRAINT FK_D0E0EF1F19EB6921');
        $this->addSql('ALTER TABLE project_client DROP CONSTRAINT FK_D0E0EF1F166D1F9C');
        $this->addSql('ALTER TABLE project_service DROP CONSTRAINT FK_778396B7166D1F9C');
        $this->addSql('ALTER TABLE query_criteria DROP CONSTRAINT FK_63357054EF946F99');
        $this->addSql('ALTER TABLE response DROP CONSTRAINT FK_3E7B0BFBEF946F99');
        $this->addSql('ALTER TABLE response_criteria DROP CONSTRAINT FK_1097DFCFBF32840');
        $this->addSql('ALTER TABLE project_service DROP CONSTRAINT FK_778396B7ED5CA9E6');
        $this->addSql('ALTER TABLE query DROP CONSTRAINT FK_24BDB5EBED5CA9E6');
        $this->addSql('ALTER TABLE service_attribute DROP CONSTRAINT FK_A2EBD5B1ED5CA9E6');
        $this->addSql('DROP SEQUENCE client_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE project_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE project_client_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE project_service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE query_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE query_criteria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE response_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE response_criteria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_attribute_id_seq CASCADE');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_client');
        $this->addSql('DROP TABLE project_service');
        $this->addSql('DROP TABLE query');
        $this->addSql('DROP TABLE query_criteria');
        $this->addSql('DROP TABLE response');
        $this->addSql('DROP TABLE response_criteria');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE service_attribute');
    }
}
