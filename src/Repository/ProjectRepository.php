<?php
namespace App\Repository;



use App\Entity\Client;
use App\Entity\Project;
use App\Entity\ProjectClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findByCode($code)
    {
        return parent::findOneBy(['code' => $code]);
    }


    /**
     * Возвращает сущность доступа клиента к проекту
     *
     * @param Project $project
     * @param Client $client
     * @return object|null
     */
    public function getClientAccess(Project $project, Client $client)
    {
        return $this->getEntityManager()->getRepository(ProjectClient::class)->findOneBy([
            'project'    => $project->getId(),
            'client'     => $client->getId()
        ]);
    }
}