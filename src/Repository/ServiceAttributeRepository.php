<?php
namespace App\Repository;



use App\Entity\Project;
use App\Entity\ServiceAttribute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

class ServiceAttributeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceAttribute::class);
    }

    public function findExistent($project, $service, $code)
    {
        $queryBuilder = $this->createQueryBuilder('sa');
        try {
            $attributes = $queryBuilder->select('sa')
                ->join('sa.service', 's')
                ->join('s.projectServices', 'ps')
                ->join('ps.project', 'p')
                ->where('sa.code = :code AND s.code = :service AND p.code = :project')
                ->setParameter('code', $code)
                ->setParameter('service', $service)
                ->setParameter('project', $project)
                ->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }

        return $attributes;

        return parent::findOneBy(['code' => $code]);
    }
}