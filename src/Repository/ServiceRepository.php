<?php
namespace App\Repository;

use App\Entity\Service;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    public function findExistent($project, $code)
    {
        $queryBuilder = $this->createQueryBuilder('s');
        try {
            $service = $queryBuilder->select('s')
                ->join('s.projectServices', 'ps')
                ->join('ps.project', 'p')
                ->where('s.code = :code AND p.code = :project')
                ->setParameter('code', $code)
                ->setParameter('project', $project)
                ->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }

        return $service;
    }
}