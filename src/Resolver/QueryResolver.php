<?php
namespace App\Resolver;


use App\Entity\Query;
use App\Entity\Service;
use App\Enum\QueryCriteriaTypeList;
use App\Exception\Processor\NotResolveQueryException;
use App\Traits\CheckStrGenerateTrait;
use Symfony\Component\HttpFoundation\Request;

class QueryResolver
{
    use CheckStrGenerateTrait;

    /**
     * @param Service $service
     * @param Request $request
     * @return Query
     * @throws NotResolveQueryException
     */
    public function resolveByRequest(Service $service, Request $request)
    {
        // Получим все запросы, проверим, что совпадает с request
        foreach ($service->getQueries() as $query) {
            if (!$query->getActive()) {
                continue;
            }
            if ($this->isMatch($query, $request)) {
                return $query;
            }
        }

        throw new NotResolveQueryException();
    }


    /**
     *
     * Определяем может ли обработать запись запрос пользоввателя
     * @param Query $query
     * @param Request $request
     * @return bool
     */
    private function isMatch(Query $query, Request $request)
    {
        $checkStrt = $this->getCheckStr($request);

        // Проверим на урл
        if ($query->getUri()) {
            $checkUrl = str_replace('/','\/', $query->getUri());
            if (!preg_match('/' . $checkUrl . '/',$request->get('uri'))) {
                return false;
            }
        }

        foreach ($query->getFilters() as $criteria) {
            switch ($criteria->getType()) {
                case QueryCriteriaTypeList::REGEX:
                    if (
                        !preg_match($criteria->getValue(), $checkStrt)
                    ) {
                        return false;
                    }
                    break;
            }
        }

        return true;
    }
}