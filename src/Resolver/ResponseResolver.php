<?php
namespace App\Resolver;


use App\Entity\Query;
use App\Entity\Response;
use App\Enum\ResponseCriteriaTypeList;
use App\Exception\Processor\NotResolveResponseException;
use App\Traits\CheckStrGenerateTrait;
use Symfony\Component\HttpFoundation\Request;

class ResponseResolver
{
    use CheckStrGenerateTrait;

    /**
     * @param Query $query
     * @param Request $request
     * @return Response
     * @throws NotResolveResponseException
     */
    public function resolveByRequest(Query $query, Request $request)
    {
        // Получим все запросы, проверим, что совпадает с request
        $defaultResponse = null;
        foreach ($query->getResponses() as $response) {
            if (!$response->isActive()) {
                continue;
            }

            if ($response->getIsDefault()) {
                $defaultResponse = $response;
                continue;
            }

            if ($this->isMatch($response, $request)) {
                return $response;
            }
        }

        if (!is_null($defaultResponse)) {
            return $defaultResponse;
        }

        throw new NotResolveResponseException();
    }


    /**
     *
     * Определяем может ли обработать запись запрос пользоввателя
     * @param Query $query
     * @param Request $request
     * @return bool
     */
    private function isMatch(Response $response, Request $request)
    {
        $checkStrt = $this->getCheckStr($request);

        foreach ($response->getFilters() as $criteria) {
            switch ($criteria->getType()) {
                case ResponseCriteriaTypeList::REGEX:
                    if (
                    !preg_match($criteria->getValue(), $checkStrt)
                    ) {
                        return false;
                    }
                    break;
            }
        }

        return true;
    }
}
