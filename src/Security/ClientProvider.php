<?php
namespace App\Security;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Провайдер для basic авторизации клиенто при запросах на сервис
 *
 * Class ClientProvider
 * @package App\Security
 */
class ClientProvider implements UserProviderInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function loadUserByUsername($username)
    {
        return $this->em->getRepository(Client::class)->findOneBy(['basic_login' => $username]);
    }

    public function refreshUser(UserInterface $user)
    {
        return $user;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
     {
         return $class === Client::class;
     }
}