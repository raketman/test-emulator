<?php
// src/Security/TokenAuthenticator.php
namespace App\Security;

use App\Entity\Client;
use App\Helper\Crypt\ClientCredentialCrypt;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class HttpBasicAuthenticator extends AbstractGuardAuthenticator
{
    /** @var ClientCredentialCrypt  */
    protected $clientCredentialCrypt;

    public function __construct(ClientCredentialCrypt $clientCredentialCrypt)
    {
        $this->clientCredentialCrypt = $clientCredentialCrypt;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        return true;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        $credential = $request->get('client_credential');


        if (false === strpos($credential, ':')) {
            $credential = $this->clientCredentialCrypt->decode($credential);
        }
        @list ($login, $password) = explode(':', $credential);

        return [
            'login'     => $login ?? '',
            'password'  => $password ?? ''
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var Client $client */
        $client = $userProvider->loadUserByUsername($credentials['login']);
        
        if (null === $client) {
            return null;
        }

        // Сверим пароль
        if ($credentials['password'] !== $client->getBasicPassword()) {
            return null;
        }

        return $client;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case

        // return true to cause authentication success
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new \App\Exception\Authorization\AuthenticationException();
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new \App\Exception\Authorization\AuthenticationException();
    }

    public function supportsRememberMe()
    {
        return false;
    }
}