<?php
namespace App\Security\Voter;

use App\Entity\Client;
use App\Entity\Project;
use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ProjectVoter extends Voter
{
    const DEFINITION = 'definition';

    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::DEFINITION])) {
            return false;
        }

        if (!$this->prepareSubject($subject) instanceof Project) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param Project $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Client $user */
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::DEFINITION:
                $access = $this->em->getRepository(Project::class)->getClientAccess($this->prepareSubject($subject), $user);

                return !is_null($access);
                break;
        }

        return false;
    }

    private function prepareSubject($subject)
    {
        if ($subject instanceof Project) {
            return $subject;
        }

        return $this->em->getRepository(Project::class)->findOneBy(['code' => $subject]);
    }
}