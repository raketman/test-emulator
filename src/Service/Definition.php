<?php
namespace App\Service;


use App\Entity\Service;
use App\Entity\ServiceAttribute;
use App\Helper\Crypt\ClientCredentialCrypt;
use App\Helper\ServiceDefinitionFactory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Definition
{
    /** @var ServiceDefinitionFactory  */
    protected $factory;

    /** @var ClientCredentialCrypt  */
    protected $encoder;

    /** @var TokenStorageInterface  */
    protected $tokenStorage;

    public function __construct(
        ServiceDefinitionFactory $factory,
        ClientCredentialCrypt $encoder,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->encoder = $encoder;
        $this->factory = $factory;
    }

    /**
     * @param Service $service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\UnsupportedServiceTypeException
     */
    public function processService(Service $service)
    {
        $definition = $this->factory->createServiceDefinition($service);
        return $definition->generate($this->getClientCredential());
    }


    /**
     * @param Service $service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\UnsupportedServiceTypeException
     */
    public function processAttribute(ServiceAttribute $serviceAttribute)
    {
        $definition = $this->factory->createAttributeDefinition($serviceAttribute);

        return $definition->generate($this->getClientCredential());
    }

    protected function getClientCredential()
    {
        /** @var UserInterface $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $string = sprintf('%s:%s:%s', $user->getUsername(), $user->getPassword(), time());

        return [
            'client_token' => urlencode($this->encoder->encode($string))
        ];
    }
}