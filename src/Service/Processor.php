<?php
namespace App\Service;


use App\Entity\Query;
use App\Entity\Response;
use App\Entity\Service;
use App\Exception\Processor\NotResolveProcessException;
use App\Helper\View\ResponseView;
use App\Resolver\QueryResolver;
use App\Resolver\ResponseResolver;
use App\Traits\ErrorLoggingTrait;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class Processor
{
    use ErrorLoggingTrait;

    /** @var  QueryResolver*/
    private $queryResolver;

    /** @var ResponseResolver  */
    private $responseResolver;

    /** @var ResponseView  */
    private $responseView;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        QueryResolver $queryResolver,
        ResponseResolver $responseResolver,
        ResponseView $responseView,
        LoggerInterface $logger
    )
    {
        $this->entityManager = $entityManager;
        $this->queryResolver = $queryResolver;
        $this->responseResolver = $responseResolver;
        $this->responseView = $responseView;
        $this->logger = $logger;
    }


    /**
     * @param Service $service
     * @param Request|null $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws NotResolveProcessException
     * @throws \App\Exception\Processor\NotResolveQueryException
     * @throws \App\Exception\Processor\NotResolveResponseException
     */
    public function process(Service $service, Request $request = null)
    {
        if (!$request) {
            throw new NotResolveProcessException('Нельзя опеределить запрос');
        }

        /** @var Query $query */
        $query = $this->queryResolver->resolveByRequest($service, $request);

        /** @var Response $response */
        $response = $this->responseResolver->resolveByRequest($query, $request);

        // Время простоя
        sleep($response->getTimeout());

        // Залочи ответ, если необходимо
        if ($response->getBlockedTime()) {
            try {
                $interval = new \DateInterval('PT' . $response->getBlockedTime() . 'S');
                $response->setBlockedUntil((new \DateTime())->add($interval));
                
                $this->entityManager->persist($response);
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                $this->logger->error('save-blocked-timed', $this->formatException($e));
                throw new NotResolveProcessException($e->getMessage());
            }
        }

        return $this->responseView->render($response, $query, $service, $request);

    }
}