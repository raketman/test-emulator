<?php
namespace App\Traits;


use Sonata\AdminBundle\Route\RouteCollection;

/**
 *
 * Общие свойства дочерних админок
 * Trait AdminSideMenuTrait
 * @package App\Traits
 */
trait AdminChildTrait
{
    protected function configureRoutes(RouteCollection $collection)
    {
        if ($this->isChild()) {
            return;
        }

        // This is the route configuration as a parent
        //$collection->clear();
    }
}