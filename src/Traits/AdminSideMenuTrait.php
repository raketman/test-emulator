<?php
namespace App\Traits;


use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

/**
 *
 * Для отрисовки дополнительного меню
 * Trait AdminSideMenuTrait
 * @package App\Traits
 */
trait AdminSideMenuTrait
{

    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        /** @var AdminInterface $child */
        foreach ($this->getChildren() as $child) {
            $menu->addChild($child->getLabel(), [
                'uri' => $admin->generateUrl(sprintf('%s.list', $child->getCode()), ['id' => $id])
            ]);
        }

    }
}