<?php
namespace App\Traits;

use Symfony\Component\HttpFoundation\Request;

/**
 * Генерирует строка для проверки совпадения с ответом/запросом
 *
 * Trait CheckStrGenerateTrait
 * @package App\Traits
 */
trait CheckStrGenerateTrait
{
    /**
     * Генерирует строку для проверки на соотвествие критериям
     *
     * @param Request $request
     * @return string
     */
    protected function getCheckStr(Request $request)
    {
        return serialize($this->getRequestData($request));
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getRequestData(Request $request)
    {
        return array_merge(
            $request->headers->all(),
            $request->request->all(),
            $request->query->all(),
            [$request->get('uri')],
            $request->cookies->all(),
            [$request->getContent()],
            [
                '__uri' => $request->get('uri'),
                '__content' => $request->getContent()
            ]
        );
    }
}
