<?php
namespace App\Traits;


use Doctrine\ORM\EntityManagerInterface;

/**
 *
 * Trait EntityManagerTrait
 * @package App\Traits
 */
trait EntityManagerTrait
{

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @return $this
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }
}