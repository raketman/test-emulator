<?php
namespace App\Traits;

use Symfony\Component\HttpFoundation\Request;

/**
 * Трейт для сохранеие ошибок в лог
 *
 * Trait CheckStrGenerateTrait
 * @package App\Traits
 */
trait ErrorLoggingTrait
{
    /**
     * @param \Throwable $e
     * @return array
     */
    protected function formatException(\Throwable $e)
    {
        $result = [];

        foreach ($e as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }
}