<?php
namespace App\Traits;

/**
 *
 * Конвертирует headers из одной строки в массив ключ-значение
 * Trait HeaderParseTrait
 * @package App\Traits
 */
trait HeaderParseTrait
{
    /**
     * @param $str
     * @return array
     */
    protected function splitHeader($str)
    {
        $parts = explode("\n", $str);

        $headers = [];

        foreach ($parts as $part) {
            list($header, $value) = explode(':', $part);

            $headers[$header] = $value;
        }

        return $headers;
    }
}